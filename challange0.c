#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define City 2048//都市数

double distance(double *city1, double *city2);//2都市間の距離を測る関数

double dist[City][City];//2都市間の距離を保存するテーブル ローカル変数では都市数2048に対応できなかった

int main(void)
{   
    FILE *input, *output;//ファイルポインタ
    double city[City][3];//都市の位置情報・訪問済みフラグ配列
    int root[City];//訪問順格納配列
    double x, y;
    int ret;
    char c, c2;//捨て変数
    double shortest = 10000000;//最も短い距離を保存する変数
    int i, j, min_city, current;
    double len = 0;//移動距離の合計

    if ((input = fopen("input_6.csv", "r")) == NULL) {
        printf("file open error!!1\n");
        exit(EXIT_FAILURE);
    }

    if ((output = fopen("solution_yours_6.csv", "w")) == NULL) {
        printf("file open error!!2\n");
        exit(EXIT_FAILURE);
    }

    fscanf(input, "%c,%c", &c, &c2);//最初の一行（"x"，"y"）を読み込み
    i = 0; 
    while ((ret = fscanf(input, "%lf,%lf", &x, &y) ) != EOF){//csvファイルから位置情報を配列へ読み込み
        //printf("%lf, %lf\n", x, y);
        
        city[i][0] = x;
        city[i][1] = y;
        city[i][2] = 0;//訪問済みかチェックフラグ
        
        i++;
    }

    for(i = 0; i < City; i++){//全ての街同士の距離を測ってテーブルに格納
        for(j = 0; j < City; j++){
            if(i == j){
                dist[i][j] = 10000000;
            }
            else{
                dist[i][j] = distance(city[i], city[j]);
            }
            //printf("%lf, ", dist[i][j]);
        }
        //printf("\n");
    }
    
   
    //city0から最も近いものを選択…以下欲張り法繰り替えし
    fprintf(output, "index\n");
    root[0] = 0;//0番目の都市からスタート
    city[0][2] = 1;
    current = 0;
    for(i = 0; i < City; i++){//都市数分くりかえす
        for(j = 0; j < City; j++){//一番距離の短い都市を探す
            if (city[j][2] != 1 && shortest > dist[current][j]){
                min_city = j;
                shortest = dist[current][j];
            }
        }
        root[i + 1] = min_city;
        city[min_city][2] = 1;
        current = min_city;
        if (i + 1 != City){//最後に訪問する都市でなければ
            len += dist[root[i]][root[i + 1]];
            printf("len += %lf\n", dist[root[i]][root[i + 1]]);
        }
        else {
            len += dist[root[0]][root[i + 1]];
            printf("len += %lf\n", dist[root[0]][root[i + 1]]);
        }
        fprintf(output, "%d\n", root[i]);
        shortest = 10000000;;
    }

    printf("最短距離：%lf\n", len);

    fclose(input);
    fclose(output);

}

double distance(double *city1, double *city2){
    return sqrt(pow((city1[0] - city2[0]), 2) + pow((city1[1] - city2[1]), 2));
}
